![BEAST Auth](/beast-auth.png)

# Requirements
NodeJS v8.9.4+

# Configuration
Configure your userpool and client IDs in `config/config.yml` (if using interactive mode)

# Usage (interactive)
    npm install
    node beast-auth

# Usage (non-interactive)
    npm install
    node beast-auth --mode=json --userpool=<userpool id> --clientid=<client id> --username=<username> --password=<password>