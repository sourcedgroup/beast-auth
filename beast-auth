const YAML = require('yamljs');
const logo = require('asciiart-logo');
const pjson = require('./package.json');
const config = YAML.load('./config/config.yml');
const prompt = require('prompt');
const argv = require('yargs').argv;
var mode = "interactive";

if(argv.mode !== undefined){
    if (argv.mode != "json" && argv.mode != "interactive"){
        console.log("Invalid mode specified, supported modes : json,interactive");
        process.exit(1);   
    }
}


if(argv.mode == "json") {
    mode = "json";
    if(argv.userpool == undefined || argv.clientid == undefined || argv.username == undefined || argv.password == undefined){
        console.log("Required parameters not specified, requires userpool, clientid, username, password");
        process.exit(1);
    }
}

// greeting message
if(mode == "interactive"){
    console.log(
        logo({
          name: 'BEAST Auth',
          font: 'cyberlarge',
          lineChars: 12,
          padding: 1,
          margin: 1
        })
        .emptyLine()
        .right('version '+pjson.version)
        .emptyLine()
        .wrap('Authenticate to BEAST from the command line')
        .render()
      );
}

// check config
if (mode == "json" && argv.userpool !== undefined){
    var userpoolId = argv.userpool;
}
else {
    if (config.userpool !== undefined){
        if (config.userpool.id !== undefined){
            if(config.userpool.id !== null){
                var userpoolId = config.userpool.id    
            }
            else {
                console.log("Userpool ID not defined in config, exiting...")
                process.exit(1); 
            }
        }
    }
    else {
        console.log("Userpool not defined in config, exiting...")
        process.exit(1);   
    }
}

if (mode == "json" && argv.clientid !== undefined){
    var clientId = argv.clientid
}
else {
    if(config.userpool.clientid !== undefined ){
        if(config.userpool.clientid !== null){
            var clientId = config.userpool.clientid
        }
        else {
            console.log("Client ID not defined in config, exiting...")
            process.exit(1);    
        }
    }
    else {
        console.log("Client ID not defined in config, exiting...")
        process.exit(1);
    }
}


const poolData = {
    UserPoolId : userpoolId,
    ClientId: clientId,
    Paranoia: 10
}

const cognitoWrapper = require('cognito-user-pool')(poolData);

// input schemas
var upSchema = {
    properties: {
      username: {
        message: 'username',
        required: true
      },
      password: {
        message: 'password',
        hidden: true,
        required: true

      }
    }
  };

var npSchema = {
    properties: {
      newPassword: {
        message: 'new password',
        hidden: true,
        required: true
      },
      newPasswordConfirm: {
        message: 'confirm password',
        hidden: true,
        required: true

      }
    }
  };

var mfaSchema = {
    properties: {
        mfaToken: {
            message: 'MFA code',
            required: true
        }
    }
  };

if(mode == "interactive"){
    prompt.start();
    prompt.message = "";
    prompt.delimiter = ":";
    prompt.get(upSchema,function(err,result){
        if(err){
            console.log(err);
        }
        else {
            var userName = result.username;
            var password = result.password;
            var params = {
               username: userName,
               password: password
            };
            cognitoWrapper.login(params, function(err,data){
                if(err){
                    console.log(err);
                }
                else {
                    if(data.nextStep !== undefined){
                        switch(data.nextStep){
                            case 'NEW_PASSWORD_REQUIRED':
                                console.log("You are required to change your password on first login. ");
                                prompt.get(npSchema,function(err,result){
                                    if(err){
                                        console.log(err);
                                    }
                                    else {
                                        if(result.newPassword == result.newPasswordConfirm){
                                            password = result.newPassword;
                                            var params = {
                                                username: userName,
                                                loginSession: data.loginSession,
                                                newPassword: password
                                            }
                                            cognitoWrapper.loginNewPasswordRequired(params,function(err,data){
                                                if(err){
                                                    console.log(err);
                                                }
                                                else {
                                                    if(data.nextStep !== undefined){
                                                        switch(data.nextStep){
                                                            case 'MFA_AUTH':
                                                                prompt.get(mfaSchema,function(err,result){
                                                                    if(err){
                                                                        console.log(err);
                                                                    }
                                                                    else {
                                                                        params = {
                                                                            username: userName,
                                                                            loginSession: data.loginSession,
                                                                            mfaCode : result.mfaToken
                                                                        }
                                                                        cognitoWrapper.loginMfa(params,function(err,data){
                                                                            if(err){
                                                                                console.log(err);
                                                                            }
                                                                            else {
                                                                                var d = new Date(0);
                                                                                d.setUTCSeconds(data.idTokenExpiresAt);
                                                                                console.log("Your token is available below, please pass as Authentication header to BEAST");
                                                                                console.log("----------------------------------------------------------------------------");
                                                                                console.log(data.idToken);
                                                                                console.log("----------------------------------------------------------------------------");
                                                                                console.log("The token will expire at "+d.toString());
                                                                            }
                                                                        });
                                                                    }
                                                                })
                                                        }
                                                    }
                                                }
                                            })
                                        }
                                        else {
                                            console.log("Passwords do not match, please try again");
                                            process.exit(1);
                                        }
                                    }
                                });
                            break;
                            case 'MFA_AUTH':
                                prompt.get(mfaSchema,function(err,result){
                                    if(err){
                                        console.log(err);
                                    }
                                    else {
                                        params = {
                                            username: userName,
                                            loginSession: data.loginSession,
                                            mfaCode : result.mfaToken
                                        }
                                        cognitoWrapper.loginMfa(params,function(err,data){
                                            if(err){
                                                console.log(err);
                                            }
                                            else {
                                                var d = new Date(0);
                                                d.setUTCSeconds(data.idTokenExpiresAt);
                                                console.log("Your token is available below, please pass as Authentication header to BEAST");
                                                console.log("----------------------------------------------------------------------------");
                                                console.log(data.idToken);
                                                console.log("----------------------------------------------------------------------------");
                                                console.log("The token will expire at "+d.toString());
                                            }
                                        });
                                    }
                                })
                            break;
                        }
                    }
                }
            });        
        }
    });
}
else {
    // non-interactive mode
    var params = {
        username: argv.username,
        password: argv.password
     };
     cognitoWrapper.login(params, function(err,data){
         if(err){
             console.log(err);
             process.exit(1);
         }
         else {
             if(data.nextStep !== undefined){
                 switch(data.nextStep){
                     case 'NEW_PASSWORD_REQUIRED':
                         console.log("You are required to change your password, exiting...");
                         process.exit(1);
                     break;
                     case 'MFA_AUTH':
                        console.log("MFA code required, exiting...");
                        process.exit(1);
                     break;
                 }
             }
             else {
                 if(data.idToken !== undefined){
                    console.log(JSON.stringify({"token":data.idToken,"expiry":data.idTokenExpiresAt}));
                 }
                 else {
                    console.log("Unknown error, exiting..");
                    process.exit(1);
                 }
             }
         }
     });

}
